Time-dependent density-functional perturbation theory (TDDFPT)

Example 1: Calculation of the absorption spectrum of benzene molecule (C6H6)
           using the `turbo_davidson.x` code

           cd example1/

Example 2: Calculation of the absorption spectrum of benzene molecule (C6H6) 
           using the `turbo_lanczos.x` code

           cd example2/

Example 3: Calculation of the EELS spectra for silicon using the `turbo_eels.x` code

           cd example3/


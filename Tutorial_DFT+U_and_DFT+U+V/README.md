
This is a tutorial on how to use density-functional theory (DFT) with Hubbard functionals (DFT+U and DFT+U+V), where U and V are on-site and inter-site Hubbard parameters. It is based on the open-source [Quantum ESPRESSO distribution](https://www.quantum-espresso.org/) [1,2].

Quantum ESPRESSO (v6.7) is installed in the Quantum Mobile - a virtual machine that can run on Windows, MacOS, or Linux.

# Using the Quantum Mobile Virtual Machine

See the Instructions.pdf on how to setup the Quantum Mobile on your computer. 

# Exercises

See the Tutorial.pdf on how to do the exercises which are listed below:

 - Exercise 1 (1_DFT): Standard DFT calculation for CoO
 - Exercise 2 (2_compute_U): Calculation of the Hubbard U parameter for CoO
 - Exercise 3 (3_DFT+U): DFT+U calculation for CoO
 - Exercise 4 (4_compute_U_and_V): Calculation of the Hubbard U and V parameters for CoO
 - Exercise 5 (5_DFT+U+V): DFT+U+V calculation for CoO

# Video

The video of a similar tutorial (https://www.youtube.com/watch?v=WSABAqPWNH0) is available on YouTube on the official Quantum ESPRESSO channel (https://www.youtube.com/channel/UCApGrcNfEyEdP7IR8YPyxiQ).

# Bibliography
1. P. Giannozzi et al., J. Phys.: Condens. Matter 21, 395502 (2009).
2. P. Giannozzi et al., J. Phys.: Condens. Matter 29, 465901 (2017).
